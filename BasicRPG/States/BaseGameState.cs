﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using BeardedMonoLibrary;
using BeardedMonoLibrary.Controls;

namespace BasicRPG.States
{
    public class BaseGameState : GameState
    {

        #region Fields Region

        protected BasicGame GameRef;

        protected ControlManager ControlManager;

        protected PlayerIndex playerIndexInControl;

        #endregion

        #region Properties Region
        #endregion

        #region Constructor Region

        public BaseGameState(Game game, GameStateManager manager) : base(game, manager)
        {
            GameRef = (BasicGame)game;

            playerIndexInControl = PlayerIndex.One;
        }

        #endregion

        #region Monogame Method Region

        protected override void LoadContent()
        {
            ContentManager Content = Game.Content;

            SpriteFont menuFont = Content.Load<SpriteFont>(@"ControlFont");
            ControlManager = new ControlManager(menuFont);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        #endregion
    }
}
