﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BeardedMonoLibrary;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BasicRPG.States
{
    public class StartState : BaseGameState
    {
        #region Field Region
        #endregion

        #region Property Region
        #endregion

        #region Constructor Region

        public StartState(Game game, GameStateManager manager) : base(game, manager)
        {
        }

        #endregion

        #region Monogame Method Region

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (InputHandler.KeyReleased(Keys.Escape))
            {
                Game.Exit();
            }

            base.Draw(gameTime);
        }

        #endregion

        #region Game State Method Region
        #endregion
    }
}
