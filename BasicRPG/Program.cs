﻿using System;

namespace BasicRPG
{
#if WINDOWS || LINUX
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new BasicGame())
                game.Run();
        }
    }
#endif
}
